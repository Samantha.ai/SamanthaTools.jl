# SamanthaTools.jl
A variety of applications and tools centered around the Samantha.jl SNN engine.

## Included Applications and Utilities
* samanthad.jl - Daemon which wraps the Samantha.jl API to create, modify, and execute agents  
* samantha.jl - Command line interface to samanthad.jl  
  * Dependencies: Crayons

## Installation
Install a supported version of Julia (currently 0.6 or greater) on a supported OS, and install Samantha.jl.  
```
Pkg.clone("https://gitlab.com/Samantha.ai/Samantha.jl")
Pkg.clone("https://gitlab.com/Samantha.ai/SamanthaTools.jl")
```

## Usage
Before using the tools, the user should setup the necessary directory structure and environment variables that the tools expect:  
`source bin/setenv.sh`  
This line can be placed in the user's shell rc file (such as .bashrc) to keep from having to constantly run this file before using the tools

For the average user, the next thing to do is to start samanthad:  
`julia src/samanthad.jl`  

Samanthad is now providing an RPC socket that the rest of tools will use to control it. As an example, here is how to start the samantha CLI tool (which will automatically connect to the running samanthad instance):  
`julia src/samantha.jl`  

## Repository Layout
LICENSE.md - License file  
README.md - This file!  
REQUIRE - Julia Dependency Requirements file  
TODO.md - Todo list  
bin/ - Binaries and helper scripts  
src/ - Source code  

## Contributing Guidelines
All reasonable issues and code submissions will be given a fair chance, but duplicates may be immediately rejected. Please use the issue/PR search features to see if someone else has already made a similar submission.  
Contributions will be accepted from anyone, regardless of their identity or ideology, and whether they are an individual or organization.  
All contributed code is automatically licensed under the same license as the rest of the repository's code (please see LICENSE.md for the current license).  
Bounties are not currently present, but I am willing to post them if there is interest. Please file an issue if you are offering a bounty for a specific type of contribution.  

## Code of Conduct
Be polite and understanding. If you are rude, insulting, or complain about something without offering a reasonable suggestion or PR, I have no qualms with removing your right to post or contribute.  
Please keep all issues and PRs on-topic. Banter should be taken offline. You can find myself and other contributors on Julia Slack or Discourse.  

## License
SamanthaTools is licensed as MIT. Please see LICENSE.md for the full license terms.
