module SamanthaREPL
  import Base: LineEdit, REPL
  importall Crayons.Box

  # Code borrowed from Keno Fischer's Cxx.jl

  function createLineParser(commands, state, repl)
    function(line)
      if endswith(line, "\x04")
        close(repl.t.in_stream)
        close(repl.t.out_stream)
        throw(InterruptException)
      end
      # Parse tokens
      tokens = split(line, " ")
      length(tokens) == 0 && return
      if tokens[1] == "^"
        lastpath = ()
        shift!(tokens)
      end

      # Locate command
      # FIXME: lastpath
      lastpath = ()
      curpath = (lastpath...)
      curdict = commands
      for path in lastpath
        curdict = curdict[path]
      end
      try
        for (idx,arg) in enumerate(tokens)
          arg == "" && continue
          if !haskey(curdict, arg)
            # Invalid
            throw("Invalid command: $arg")
          end
          if curdict[arg] isa Dict
            # Descend
            curpath = (curpath..., arg)
            curdict = curdict[arg]
          elseif curdict[arg] isa String
            # Unalias
            ident = curdict[arg]
            curpath = (curpath..., ident)
            curdict = curdict[ident]
          else
            # Run command with args
            curdict[arg](state, repl, idx<length(tokens)?tokens[idx+1:end]:())
            curpath = ()
            break
          end
        end
        lastpath = (lastpath..., curpath...)
      catch err
        if err isa AssertionError
          println(state, RED_FG(err.msg))
        elseif err isa InterruptException
          rethrow(err)
        else
          println(state, RED_FG(string(err)))
        end
      end
      return nothing
    end
  end

  function CreateSamanthaREPL(commands::Dict, state; repl=Base.active_repl)
    prompt = "samantha> "
    name = :samantha
    main_mode = repl.interface.modes[1]
    mirepl = isdefined(repl,:mi) ? repl.mi : repl
    # Setup panel
    panel = LineEdit.Prompt(prompt;
      # Copy colors from the prompt object
      prompt_prefix=Base.text_colors[:light_cyan],
      prompt_suffix=Base.text_colors[:white],
      on_enter = s -> begin
        true
      end)

    panel.on_done = REPL.respond(createLineParser(commands, state, repl), repl, panel)

    main_mode == mirepl.interface.modes[1] &&
      push!(mirepl.interface.modes,panel)

    # TODO: Make julia and samantha history providers mix
    hp = main_mode.hist
    hp.mode_mapping[name] = panel
    panel.hist = hp

    search_prompt, skeymap = LineEdit.setup_search_keymap(hp)
    mk = REPL.mode_keymap(main_mode)

    b = Dict{Any,Any}[skeymap, mk, LineEdit.history_keymap, LineEdit.default_keymap, LineEdit.escape_defaults]
    panel.keymap_dict = LineEdit.keymap(b)
    
    panel
  end

  function RunSamanthaREPL(commands::Dict, state; repl=Base.active_repl)
    key = "!"
    panel = CreateSamanthaREPL(commands, state; repl=repl)
    
    mirepl = isdefined(repl,:mi) ? repl.mi : repl
    main_mode = mirepl.interface.modes[1]

    # Install this mode into the main mode
    const samantha_keymap = Dict{Any,Any}(
      key => function (s,args...)
        if isempty(s) || position(LineEdit.buffer(s)) == 0
          #info(typeof(s))
          #info(s)
          buf = copy(LineEdit.buffer(s))
          LineEdit.transition(s, panel) do
            LineEdit.state(s, panel).input_buffer = buf
          end
        else
          LineEdit.edit_insert(s,key)
        end
      end
    )
    main_mode.keymap_dict = LineEdit.keymap_merge(main_mode.keymap_dict, samantha_keymap);
    nothing
  end
end
