using Samantha
using Base.Terminals: TTYTerminal
using Base.REPL: LineEditREPL, run_repl #REPLBackendRef, start_repl_backend, run_frontend
using Crayons.Box
import Base: println

include("common.jl")
include("repl.jl")
include("commands.jl")

mutable struct InternalState
  repls::Vector{LineEditREPL}

  agents::Dict{String,Agent}
  configs::Dict{String,Any}
  statuses::Dict{String,Any}
  tasks::Dict{String,Task}

  mprofiles::Dict{String,MutationProfile}
  #eprofiles::Dict{String,EvolutionProfile}
end
InternalState() = InternalState(
  LineEditREPL[],

  Dict{String,Agent}(),
  Dict{String,Any}(),
  Dict{String,Any}(),
  Dict{String,Task}(),

  Dict{String,MutationProfile}()
)

function main()
  log_info("Starting")

  mkpath(SAMANTHA_RUN_PATH)
  
  # Setup internal state
  state = InternalState()
  
  # TODO: Redirect STDOUT/STDERR to broadcast
  #=out_r, out_w = redirect_stdout()
  @schedule begin
    while true
      data = readavailable(out_r)
      println(STDOUT, String(data))
    end
  end=#
  
  # Serve REPL sessions
  # TODO: Optionally listen on port 3582
  # TODO: Properly handle client disconnect
  srv = listen(joinpath(SAMANTHA_RUN_PATH, "samanthad.sock"))
  @schedule while true
    log_info("Listening")
    sock = accept(srv)
    log_info("Accepted connection")
    # TODO: Different term string?
    ttyterm = TTYTerminal("socket", sock, sock, sock)
    repl = LineEditREPL(ttyterm)
    push!(state.repls, repl)
    @schedule begin
      try
        @schedule SamanthaREPL.RunSamanthaREPL(commands, state; repl=repl)
        run_repl(repl)
      catch err
        if err isa InterruptException
          log_warn("Received interrupt, exiting")
          exit(0)
        # TODO: Use a less hacky method to exit cleanly
        elseif (err isa ArgumentError && err.msg == "stream is closed or unusable") || err isa Base.UVError
          log_info("Stream closed, stopping REPL")
        else
          log_err("Unkown socket error: $(typeof(err))$(isdefined(err, :msg) ? ": " * err.msg : "")))")
          rethrow(err)
        end
      end
    end
  end

  # Exit handler
  atexit() do
    log_info("Stopping")
    print("\e[2K\n")
  end

  # Wait for exit signal
  log_info("Ready")
  while true sleep(1) end
end

### Utilities ###

function println(state::InternalState, data...)
  for repl in state.repls
    println(repl, data...)
  end
end
println(repl::Base.REPL.LineEditREPL, data...) = println(repl.t.out_stream, data...)

### Internal API ###

function _agent_added(state, name)
  state.configs[name] = Dict{String,Any}(
    "fps"=>0
  )
  state.statuses[name] = Dict{String,Any}(
    "fps"=>0,
    "internalTime"=>0,
    "externalTime"=>0
  )
end
function _agent_deleted(state, name)
  delete!(state.configs, name)
  delete!(state.statuses, name)
end
function _agent_launch(state, name)
  @schedule begin
    try
      agent = state.agents[name]
      config = state.configs[name]
      status = state.statuses[name]

      t1 = time_ns()
      while true
        # Sleep if not running
        if config["fps"] == 0
          sleep(1)
          continue
        end

        # Run agent and calculate internal and external latencies
        t0 = time_ns()
        edt = t0 - t1
        run!(agent)
        t1 = time_ns()
        idt = t1 - t0
        sleep(1/config["fps"])
        #sleep(max((1 / config["fps"]) - (1/idt), 0))
        t2 = time_ns()
        isdt = t2 - t0
        fps = 1/isdt

        # Update status
        status["fps"] = fps
        status["internalTime"] = idt
        status["externalTime"] = edt
      end
    end
  end
end
function _agent_kill(state, name)
  try Base.throwto(state.tasks[name], InterruptException()) end
end

# Run
try
  main()
catch err
  if err isa InterruptException
    log_warn("Received interrupt, exiting")
    exit(0)
  end
  log_err("Error: $err")
end
