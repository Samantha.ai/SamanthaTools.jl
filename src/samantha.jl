include("common.jl")

function main()
  # Connect to samanthad socket
  if !ispath(joinpath(SAMANTHA_RUN_PATH, "samanthad.sock"))
    println("Waiting for samanthad socket, Ctrl-C to exit")
    while !ispath(joinpath(SAMANTHA_RUN_PATH, "samanthad.sock"))
      sleep(1)
    end
  end
  sock = connect(joinpath(SAMANTHA_RUN_PATH, "samanthad.sock"))
  
  # Setup terminal
  run(`stty isig -icanon -echo min 1`)
  atexit() do
    run(`stty cooked echo`)
    print("\e[2K\n")
    close(sock)
  end

  @schedule begin
    try
      while true
        data = readavailable(STDIN)
        if UInt8(data[1]) == 0x04
          println(sock, 0x04)
          exit(0)
        end
        print(sock, String(data))
      end
    catch err
      err isa InterruptException && exit(0)
      rethrow(err)
    end
  end
  @schedule begin
    try
      while true
        data = readavailable(sock)
        print(String(data))
      end
    catch err
      err isa InterruptException && exit(0)
      rethrow(err)
    end
  end
  # TODO: Return instead, to allow other code to be run
  while isopen(sock)
    try sleep(0.1) end
  end
end

### Utilities ###

# Run system editor on temporary file
function run_editor(func, data = "")
  path, f = mktemp()
  if data != ""
    write(f, data)
  end
  editor = Base.get(ENV, "EDITOR", "vi")
  try
    run(`$editor $path`)
    func(path, f)
  catch err
    rethrow(err)
  finally
    close(f)
  end
end

# Run system pager on data
function run_editor(data)
  path, f = mktemp()
  write(f, data)
  pager = Base.get(ENV, "PAGER", "less")
  run(`$pager $path`)
  result = include(path)
  close(f)
  return result
end

# Run
main()
