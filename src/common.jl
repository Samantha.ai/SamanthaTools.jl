include("logging.jl")

# Read env vars
const SAMANTHA_DATA_PATH = get(ENV, "SAMANTHA_DATA_PATH", pwd())
const SAMANTHA_RUN_PATH = get(ENV, "SAMANTHA_RUN_PATH", pwd())
const SAMANTHA_LOG_PATH = get(ENV, "SAMANTHA_LOG_PATH", pwd())

# Exit on interrupt
ccall(:jl_exit_on_sigint, Void, (Cint,), 0)
