function log_writeln(args...)
  write(STDOUT, args..., '\n')
  flush(STDOUT)
end
log_info(args...) = log_writeln("\e[1;36m[$(now())] \e[0;36m", args..., "\e[0m")
log_warn(args...) = log_writeln("\e[1;33m[$(now())] \e[0;33m", args..., "\e[0m")
log_err(args...) = log_writeln("\e[1;31m[$(now())] \e[0;31m", args..., "\e[0m")
