#= Command List =#
#=
agent (ag)
  list (l)
  create (c)
  delete (d)
  loadfile (lf)
  storefile (sf)
  run (r)
  stop (s)
  merge (mg)
  clone (cl)
  addnode (an)
  addedge (ae)
  delnode (dn)
  deledge (de)
  addhook (ah)
  delhook (dh)
mutation (mu)
  list (l)
  create (c)
  delete (d)
# TODO:
  addmutation (am)
  delmutation (dm)
evolution (ev)
  list (l)
  create (c)
  delete (d)
  addfactor (af)
  delfactor (df)
=#

### Commands ###

commands = Dict()

### General ###

commands["quit"] = (state, repl, args) -> begin
  close(repl.t.in_stream)
  close(repl.t.out_stream)
  throw(InterruptException())
end

### Agent ###

# TODO: Colors!
# TODO: Verify arguments and provide nicer errors
commands["agent"] = Dict()
commands["ag"] = "agent"
commands["agent"]["list"] = (state, repl, args) -> begin
  # TODO: Shorten and cleanup
  agents = Dict()
  for (name,agent) in state.agents
    agents[name] = Dict(
      :nodes=>length(agent.nodes),
      :edges=>length(agent.edges),
      :running=>haskey(state.tasks, name),
      :crashed=>(haskey(state.tasks, name) ? state.tasks[name].state==:failed : false)
    )
  end
  if length(agents) == 0
    println(repl, RED_FG(BOLD("No agents")))
  else
    println(repl, GREEN_FG(BOLD("Agents ($(length(agents))):")))
    for (name,agent) in agents
      str = "$name " * string(BOLD(agent[:running] ? (agent[:crashed] ? RED_FG("xxx ") : GREEN_FG(">>> ")) : YELLOW_FG("--- ")))
      str *= "($(agent[:nodes]) nodes, $(agent[:edges]) edges)"
      println(repl, str)
    end
  end
end
commands["agent"]["show"] = (state, repl, args) -> begin
  agent = args[1]
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  # FIXME
end
commands["agent"]["create"] = (state, repl, args) -> begin
  agent = args[1]
  @assert !haskey(state.agents, agent) "Agent $agent already exists"
  println(repl, "Creating agent $(args[1])")
  state.agents[agent] = Agent()
  _agent_added(state, agent)
end
commands["agent"]["delete"] = (state, repl, args) -> begin
  agent = args[1]
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  println(repl, "Deleting agent $(args[1])")
  _agent_kill(state, agent)
  delete!(state.agents, agent)
  _agent_deleted(state, agent)
end
commands["agent"]["loadfile"] = (state, repl, args) -> begin
  agent, path = args[1:2]
  path = joinpath(SAMANTHA_DATA_PATH, path)
  @assert ispath(path) "Path $path does not exist"
  println(repl, "Loading agent $(args[1]) from $(args[2])")
  state.agents[agent] = loadfile(path)
  _agent_added(state, agent)
end
commands["agent"]["storefile"] = (state, repl, args) -> begin
  agent, path = args[1:2]
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  println(repl, "Storing agent $(args[1]) to $(args[2])")
  storefile!(path, state.agents[agent])
end
commands["agent"]["start"] = (state, repl, args) -> begin
  agent = args[1]
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  @assert !haskey(state.tasks, agent) "Agent $agent already started"
  println(repl, "Starting agent $(args[1])")
  state.tasks[agent] = _agent_launch(state, agent)
end
commands["agent"]["stop"] = (state, repl, args) -> begin
  agent = args[1]
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  @assert haskey(state.tasks, agent) "Agent $agent already stopped"
  println(repl, "Stopping agent $(args[1])")
  _agent_kill(state, agent)
  delete!(state.tasks, agent)
end
commands["agent"]["getconfig"] = (state, repl, args) -> begin
  agent, key = args[1], get(args, 2, nothing)
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  value = if key != nothing
    @assert haskey(state.configs[agent], key) "Config for $agent does not have key $key"
    state.configs[agent][key]
  else
    state.configs[agent]
  end
  println(repl, args[2], ": ", value)
end
commands["agent"]["setconfig"] = (state, repl, args) -> begin
  agent, key, value = args[1], args[2], parse(args[3])
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  state.configs[agent][key] = value
end
commands["agent"]["getstatus"] = (state, repl, args) -> begin
  agent, key = args[1], get(args, 2, nothing)
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  value = if key != nothing
    @assert haskey(state.statuses[agent], key) "Status for $agent does not have key $key"
    state.statuses[agent][key]
  else
    state.statuses[agent]
  end
  println(repl, args[2], ": ", value)
end
commands["agent"]["merge"] = (state, repl, args) -> begin
  agent1, agent2 = args[1:2]
  @assert haskey(state.agents, agent1) "Agent $agent1 does not exist"
  @assert haskey(state.agents, agent2) "Agent $agent2 does not exist"
  @assert !haskey(state.tasks, agent1) "Agent $agent1 is currently running"
  @assert !haskey(state.tasks, agent2) "Agent $agent2 is currently running"
  println(repl, "Merging agent $agent1 into $agent2")
  merge!(state.agents[agent1], state.agents[agent2])
  delete!(state.agents, agent2)
  _agent_deleted(state, agent2)
end
commands["agent"]["clone"] = (state, repl, args) -> begin
  agent1, agent2 = args[1:2]
  @assert haskey(state.agents, agent1) "Agent $agent1 does not exist"
  @assert !haskey(state.agents, agent2) "Agent $agent2 already exists"
  println(repl, "Cloning agent $agent1 to $agent2")
  state.agents[agent2] = deepcopy(state.agents[agent1])
  _agent_added(state, agent2)
end
commands["agent"]["addnode"] = (state, repl, args) -> begin
  agent = args[1]
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  # FIXME: Switch to julia repl
  # FIXME: Register to receive node on return
  @assert isdefined(Main, :ans) "No previous value in julia REPL buffer"
  @assert typeof(Main.ans) <: AbstractNode "Previous value is not a subtype of AbstractNode"
  node = Main.ans
  println(repl, "Adding node TODO to agent $agent")
  id = addnode!(state.agents[agent], node)
  println(repl, "Node ID: $id")
end
commands["agent"]["delnode"] = (state, repl, args) -> begin
  agent, node = args[1], String(args[2])
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  println(repl, "Deleting node $node from agent $agent")
  delnode!(state.agents[agent], node)
end
commands["agent"]["addedge"] = (state, repl, args) -> begin
  agent, edge, params = args[1], args[2], args[3:4]
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  println(repl, "Adding edge $(join((edge, params...), ":")) to agent $agent")
  addedge!(state.agents[agent], edge, params...)
end
commands["agent"]["deledge"] = (state, repl, args) -> begin
  agent, edge, params = args[1:4]
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  println(repl, "Deleting edge $(join((edge, params...), ":")) from agent $agent")
  deledge!(state.agents[agent], params...)
end
commands["agent"]["addhook"] = (state, repl, args) -> begin
  agent, hook, path = args[1], args[2], args[3:end]
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  println(repl, "Adding hook $hook to agent $agent with path $(join(path,"->"))")
  addhook!(state.agents[agent], hook, path)
end
commands["agent"]["delhook"] = (state, repl, args) -> begin
  agent, hook = args[1:2]
  @assert haskey(state.agents, agent) "Agent $agent does not exist"
  println(repl, "Deleting hook $hook from agent $agent")
  delhook!(state.agents[agent], hook)
end

### Mutation ###

commands["mutation"] = Dict()
commands["mu"] = "mutation"
commands["mutation"]["list"] = (state, repl, args) -> begin
  mprofiles = Dict()
  for (name,mprof) in state.mprofiles
    mprofiles[name] = Dict(
      :mutations=>length(mprof.mutations)
    )
  end
  if length(profiles) == 0
    println(repl, RED_FG(BOLD("No mutation profiles")))
  else
    println(repl, GREEN_FG(BOLD("Mutation profiles ($(length(profiles))):")))
    for (name,profile) in profiles
      str = "$name " * "($(profile[:mutations]) mutations)"
      println(repl, str)
    end
  end
end
commands["mutation"]["create"] = (state, repl, args) -> begin
  name = args[1]
  println(repl, "Creating mutation profile $name")
  @assert !haskey(state.mprofiles, name) "Mutation profile $name already exists"
  state.mprofiles[name] = MutationProfile()
end
commands["mutation"]["delete"] = (state, repl, args) -> begin
  name = args[1]
  println(repl, "Deleting mutation profile $name")
  @assert haskey(state.mprofiles, name) "Mutation profile $name does not exist"
  # FIXME: Assert no eprofiles point to this mprofile
  delete!(state.mprofiles, name)
end
