#!/bin/bash

SAMANTHA_HOME=$HOME/.samantha
mkdir -p $SAMANTHA_HOME
mkdir -p $SAMANTHA_HOME/data
mkdir -p $SAMANTHA_HOME/logs
if [ -d /tmp ]; then
  mkdir -p /tmp/samantha
  export SAMANTHA_RUN_PATH=/tmp/samantha
else
  mkdir -p $SAMANTHA_HOME/run
  export SAMANTHA_RUN_PATH=$SAMANTHA_HOME/run
fi
export SAMANTHA_DATA_PATH=$SAMANTHA_HOME/data
export SAMANTHA_LOG_PATH=$SAMANTHA_HOME/logs
